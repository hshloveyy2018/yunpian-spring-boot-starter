# yunpian-spring-boot-starter

#### 介绍
`yunpian-spring-boot-starter`,快速实现云片短信发送功能

#### 软件架构
软件架构说明


#### 安装教程/使用说明

1.  将项目导入IDEA
2.  在自己的工程引入依赖

```xml
<dependency>
    <groupId>com.hym.sms</groupId>
    <artifactId>yunpian-spring-boot-starter</artifactId>
    <version>1.0.0</version>
</dependency>
```
3.  填写云片apiKey
```
# 短信参数
yunpian: 
    api-key: 686d727f21575049c0e8b18250012ac7
```
4.  注入实例
```java
@Resource
private IYunPianService yunPianService;
```
5.  使用
```java
Map<String, Object> params = new HashMap<>();
params.put("code", "123456");
yunPianService.sendSmsByTemplate("150xxxx9682", "4166154", params);
```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

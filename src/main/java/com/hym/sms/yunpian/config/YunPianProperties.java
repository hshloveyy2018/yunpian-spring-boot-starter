package com.hym.sms.yunpian.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "yunpian")
public class YunPianProperties {

    private String charset;

    private String url;

    private String apiKey;

    private String marketingApiKey;

    private String singleSendUrl;

    private String batchSendUrl;

    private String tplSingleSendUrl;

    private String tplAddUrl;

    private String tplBatchSendUrl;

    private String tplGetUrl;

    private String tplUpdateUrl;

    private String tplDelUrl;

    private String signAddUrl;

    private String signGetUrl;

    private String signUpdateUrl;

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getMarketingApiKey() {
        return marketingApiKey;
    }

    public void setMarketingApiKey(String marketingApiKey) {
        this.marketingApiKey = marketingApiKey;
    }

    public String getSingleSendUrl() {
        return singleSendUrl;
    }

    public void setSingleSendUrl(String singleSendUrl) {
        this.singleSendUrl = singleSendUrl;
    }

    public String getBatchSendUrl() {
        return batchSendUrl;
    }

    public void setBatchSendUrl(String batchSendUrl) {
        this.batchSendUrl = batchSendUrl;
    }

    public String getTplSingleSendUrl() {
        return tplSingleSendUrl;
    }

    public void setTplSingleSendUrl(String tplSingleSendUrl) {
        this.tplSingleSendUrl = tplSingleSendUrl;
    }

    public String getTplAddUrl() {
        return tplAddUrl;
    }

    public void setTplAddUrl(String tplAddUrl) {
        this.tplAddUrl = tplAddUrl;
    }

    public String getTplBatchSendUrl() {
        return tplBatchSendUrl;
    }

    public void setTplBatchSendUrl(String tplBatchSendUrl) {
        this.tplBatchSendUrl = tplBatchSendUrl;
    }

    public String getTplGetUrl() {
        return tplGetUrl;
    }

    public void setTplGetUrl(String tplGetUrl) {
        this.tplGetUrl = tplGetUrl;
    }

    public String getTplUpdateUrl() {
        return tplUpdateUrl;
    }

    public void setTplUpdateUrl(String tplUpdateUrl) {
        this.tplUpdateUrl = tplUpdateUrl;
    }

    public String getTplDelUrl() {
        return tplDelUrl;
    }

    public void setTplDelUrl(String tplDelUrl) {
        this.tplDelUrl = tplDelUrl;
    }

    public String getSignAddUrl() {
        return signAddUrl;
    }

    public void setSignAddUrl(String signAddUrl) {
        this.signAddUrl = signAddUrl;
    }

    public String getSignGetUrl() {
        return signGetUrl;
    }

    public void setSignGetUrl(String signGetUrl) {
        this.signGetUrl = signGetUrl;
    }

    public String getSignUpdateUrl() {
        return signUpdateUrl;
    }

    public void setSignUpdateUrl(String signUpdateUrl) {
        this.signUpdateUrl = signUpdateUrl;
    }
}

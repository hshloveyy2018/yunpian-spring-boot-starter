package com.hym.sms.yunpian.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.hym.sms.yunpian.config.YunPianProperties;
import com.hym.sms.yunpian.utils.RegexUtils;

@Component
public class YunPianService implements IYunPianService {

    private static final Logger logger = LoggerFactory.getLogger(YunPianService.class);

    private static final String APIKEY = "apikey";

    private YunPianProperties yunPianProperties;

    @Autowired
    private RestTemplate restTemplate;

    public YunPianService(YunPianProperties yunPianProperties) {
        this.yunPianProperties = yunPianProperties;
    }

    /**
     * 通过模板id发送短信
     * 
     * @Title sendSmsByTemplate
     * @Auther heshaohua
     * @Date 2019年4月11日 下午5:12:20
     * @param mobile
     * @param tplId
     * @param params
     */
    @Override
    public void sendSmsByTemplate(String mobile, String tplId, Map<String, Object> params) {
        if (!RegexUtils.checkMobile(mobile)) {
            logger.error("手机号码{}格式错误", mobile);
            throw new IllegalArgumentException(String.format("手机号码%s格式错误", mobile));
        }
        Assert.hasText(tplId, "模板id不能为空");

        try {
            List<NameValuePair> args = setParams(mobile, tplId, getTplValue(params));
            // logger.info("向手机号[{}]发送短信 => {} ", mobile, JsonUtil.toString(params));
            // HttpUtil.httpPost(yunPianProperties.getTplSingleSendUrl(), args);

            HttpHeaders headers = new HttpHeaders();
            // 请勿轻易改变此提交方式，大部分的情况下，提交方式都是表单提交
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            // 封装参数，千万不要替换为Map与HashMap，否则参数无法传递
            MultiValueMap<String, String> p = new LinkedMultiValueMap<>();
            args.forEach(item -> {
                p.add(item.getName(), item.getValue());
            });
            HttpEntity<MultiValueMap<String, String>> requestEntity =
                new HttpEntity<MultiValueMap<String, String>>(p, headers);

            restTemplate.postForEntity(yunPianProperties.getTplSingleSendUrl(), requestEntity, String.class);
            // logger.info(HttpUtil.httpPostIgnoreStatus(yunPianProperties.getTplSingleSendUrl(), args));
        } catch (Exception e) {
            logger.error("短信发送失败: {}", e);
        }
    }

    /**
     * 组装apikey参数
     * 
     * @Title setParams
     * @Auther heshaohua
     * @Date 2019年4月11日 下午5:12:31
     * @param mobile
     * @param tplId
     * @param tplValue
     * @return
     */
    private List<NameValuePair> setParams(String mobile, String tplId, String tplValue) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("mobile", mobile));
        params.add(new BasicNameValuePair("tpl_id", tplId));
        params.add(new BasicNameValuePair("tpl_value", tplValue));
        params.add(new BasicNameValuePair(APIKEY, this.getApiKey(tplId)));
        return params;
    }

    /**
     * 获取模板值
     * 
     * @Title getTplValue
     * @Auther heshaohua
     * @Date 2019年4月11日 下午5:49:02
     * @param params
     * @return
     */
    private String getTplValue(Map<String, Object> params) {
        if (params == null) {
            return null;
        }
        StringBuffer tplValue = new StringBuffer();
        params.forEach((key, value) -> {
            try {
                tplValue.append(URLEncoder.encode("#" + key + "#", yunPianProperties.getCharset())).append("=")
                    .append(URLEncoder.encode(value == null ? "" : value.toString(), yunPianProperties.getCharset()))
                    .append("&");
            } catch (Exception e) {
                logger.error("获取模板值错误: {}", e);
            }
        });

        return tplValue.toString();
    }

    @Override
    public String addTpl(String tplContent, Integer notifyType) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair(APIKEY, yunPianProperties.getApiKey()));
        params.add(new BasicNameValuePair("tpl_content", tplContent));
        params.add(new BasicNameValuePair("notifyType", notifyType == null ? "0" : String.valueOf(notifyType)));

        String result = null;
        try {
            restTemplate.postForEntity(yunPianProperties.getTplAddUrl(), params, Map.class);
            // result = HttpUtil.httpPost(yunPianProperties.getTplAddUrl(), params);
        } catch (Exception e) {
            throw new RuntimeException("云片短信调用异常");
        }

        return result;
    }

    @Override
    public String findTpls() {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair(APIKEY, yunPianProperties.getApiKey()));

        String result = null;
        try {
            restTemplate.postForEntity(yunPianProperties.getTplGetUrl(), params, Map.class);
            // result = HttpUtil.httpPost(yunPianProperties.getTplGetUrl(), params);
        } catch (Exception e) {
            throw new RuntimeException("云片短信调用异常");
        }
        return result;
    }

    @Override
    public String findSigns() {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair(APIKEY, yunPianProperties.getApiKey()));

        String result = null;
        try {
            restTemplate.postForEntity(yunPianProperties.getSignGetUrl(), params, Map.class);
            // result = HttpUtil.httpPost(yunPianProperties.getSignGetUrl(), params);
        } catch (Exception e) {
            throw new RuntimeException("云片短信调用异常");
        }
        return result;
    }

    @Override
    public String sendSingleSms(String mobile, String msg) {

        if (!RegexUtils.checkMobile(mobile)) {
            logger.error("手机号码{}格式错误", mobile);
            throw new IllegalArgumentException(String.format("手机号码%s格式错误", mobile));
        }

        String result = null;
        try {

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair(APIKEY, yunPianProperties.getApiKey()));
            params.add(new BasicNameValuePair("mobile", mobile));
            params.add(new BasicNameValuePair("text", msg));

            // logger.info("向手机号[{}]发送短信 => {} ", mobile, JsonUtil.toString(params));

            restTemplate.postForEntity(yunPianProperties.getSingleSendUrl(), params, Map.class);
            // result = HttpUtil.httpPost(yunPianProperties.getSingleSendUrl(), params);
            return result;
        } catch (Exception e) {
            logger.error("短信发送失败: {}", e);
        }
        return null;
    }

    private String getApiKey(String templateId) {
        return yunPianProperties.getApiKey();
    }
}

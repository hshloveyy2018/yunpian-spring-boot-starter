package com.hym.sms.yunpian.service;

import java.util.Map;

public interface IYunPianService {

    /**
     * 云片网按模板发送短信
     * 
     * @Title sendSmsByTemplate
     * @Auther heshaohua
     * @Date 2019年4月12日 上午8:55:16
     * @param mobile
     * @param tplId
     * @param params
     */
    void sendSmsByTemplate(String mobile, String tplId, Map<String, Object> params);

    /**
     * 添加短信模板
     * 
     * @Title addTpl
     * @Auther heshaohua
     * @Date 2019年4月12日 上午9:03:30
     * @param tplContent 审核模板内容
     * @param notifyType 审核结果短信通知的方式
     *        <li>0 - 需要通知,默认
     *        <li>1 - 仅审核不通过时通知
     *        <li>2 - 仅审核通过时通知
     *        <li>3 - 不需要通知
     * @return
     */
    String addTpl(String tplContent, Integer notifyType);

    /**
     * 获取模板
     * 
     * @Title findTpls
     * @Auther heshaohua
     * @Date 2019年4月12日 上午9:35:49
     * @return
     */
    String findTpls();

    /**
     * 获取短信签名
     * 
     * @Title findSigns
     * @Auther heshaohua
     * @Date 2019年4月12日 上午9:37:19
     * @return
     */
    String findSigns();

    /**
     * 单条发送
     * 
     * @Title sendSingleSms
     * @Auther heshaohua
     * @Date 2019年4月12日 上午9:39:32
     * @param phone
     * @param msg
     * @return
     */
    String sendSingleSms(String phone, String msg);

}
